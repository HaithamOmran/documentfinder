﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnterKeyPressed : MonoBehaviour {

    InputField inputtext;

    // Use this for initialization
    void Start () {
        inputtext = GameObject.Find("InputField").GetComponent<InputField>();
	}
	
	// Update is called once per frame
	void Update () {

     
        if ((Input.GetKeyDown(KeyCode.Return)||Input.GetKeyDown(KeyCode.KeypadEnter))&& inputtext.text.Length != 0)
        {
            GameObject.Find("OutputText").GetComponent<regexmatch>().InputSent();   //Send Search to script
            inputtext.text = "";                                                    //Clear Search box
        }

	}
}
