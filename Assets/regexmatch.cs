﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;
using System.Net;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;


public class regexmatch : MonoBehaviour {

    string linkoutput;
    string tickersymbol;
    string docnumber;
    public string priordate;
    private int SuccessCount = 0;
    private int FailCount;
    private string FailList;
    float progress;
    public GameObject prefab;
    GameObject outputui;
    GameObject contentarea;
    GameObject Outputgroup;
    List<string> DLlist;

    
    


    void Awake()
    {

       outputui = GameObject.Find("OutputUI");
       outputui.GetComponent<Canvas>().enabled = false;
        contentarea = GameObject.Find("Content");
    }

   public void InputSent()
    {
        GameObject.Find("OutputText").GetComponent<Text>().text = "Please Wait. Finding Documents";

        //Get user input
        tickersymbol = GameObject.Find("InputText").GetComponent<Text>().text;              //Ticker Symbol
        docnumber = GameObject.Find("DropdownLabel").GetComponent<Text>().text;                     //# of documents requested
        docnumber = docnumber.Replace("Last", "").Replace(" ", "").Replace("Documents", "");
        Debug.Log(docnumber);

        linkoutput = "";
        FailList = "";
        FailCount = 0;
        SuccessCount = 0;
        

        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.05f);
        StartCoroutine(Search());
    }


    IEnumerator Search()
    {


        
        WebClient w = new WebClient();
       string text = w.DownloadString("http://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="+ tickersymbol + "&type=10&dateb="+priordate +"&owner=exclude&count=" + docnumber);
       // string text = w.DownloadString("file:///E:/Haitham/Documents/New%20folder/EDGAR%20Search%20Results.htm");
        string finddocuments = @"([0-9]{2}\-[QK\/A]*)</td>[\s]*[\<\>\w ="":/\.]*(Archives/edgar/data/.*\.htm)[\S\w ]*\s*[\S\w ]*\s*<td>([0-9]{4}-[0-9]{2}-[0-9]{2})"; // (Archives/edgar/data/(.*).htm)[\s\w<>""=:\/\.&;\-?|^\r]*";//<td>[0-9]{4}-[0-9]{2}-[0-9]{2}</td>";
        string Companyname = @"companyName"">[\&;\w\. 0-9]*";


        Regex r = new Regex(finddocuments, RegexOptions.IgnoreCase);
        Regex cr = new Regex(Companyname, RegexOptions.IgnoreCase);

        Match cn = cr.Match(text);
        Debug.Log(Regex.Replace(cn.ToString(), @"companyName"">", ""));
        GameObject.Find("CompanyName").GetComponent<Text>().text = Regex.Replace(cn.ToString(), @"(companyName"">)|(amp;)", "") + "\n";

        // Match the regular expression pattern against a text string.

        MatchCollection m = r.Matches(text);
        var matches = new string[m.Count];
        var arr = m.Cast<Match>().Select(ma => ma.Groups[2].Value).ToArray();
        var docdate = m.Cast<Match>().Select(ma => ma.Groups[3].Value).ToArray();
        var doctype = m.Cast<Match>().Select(ma => ma.Groups[1].Value).ToArray();

        int total = m.Count;
            
            
            for (int i = 0; i <= total - 1; i++)
            {

            progress = Mathf.Round((((float)i + 1) / ((float)total - 0) * 100));  //Gets progress of document finding crap

            // SETS output Stuff--------------
            Outputgroup = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
            Outputgroup.transform.SetParent(contentarea.transform,false);
            Outputgroup.GetComponentInChildren<Text>().text = "http://www.sec.gov/" + arr[i];
            Outputgroup.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, -((Outputgroup.GetComponent<RectTransform>().rect.size.y/2)+Outputgroup.GetComponent<RectTransform>().rect.size.y * i) , 0);
            OutputtextGroup.year = docdate[i];
            OutputtextGroup.filingtype = doctype[i];
            //--------------------------------

            GameObject.Find("OutputText").GetComponent<Text>().text = "Found " + SuccessCount + " out of " + (total) + "\n"+ progress + "%"; //linkoutput + " \n";
            yield return new WaitForSeconds(.01f);

        }
        w.Dispose();

        GameObject.Find("OutputText").GetComponent<Text>().text = "";                                                                                       //clear Text from OG output
        contentarea.GetComponent<RectTransform>().sizeDelta = new Vector3(0, (Outputgroup.GetComponent<RectTransform>().rect.size.y / 2) 
                                                                          +(Outputgroup.GetComponent<RectTransform>().rect.size.y * total), 0);             //Set size of content area based on text groups
        outputui.GetComponent<Canvas>().enabled = true;                                                                                                     //Display Text clones
        contentarea.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);



    }

    //Remove all this crap
    void Clear()
    {
        GameObject.Find("OutputText").GetComponent<Text>().text = "";
        GameObject.Find("InputText").GetComponent<Text>().text = "";


        //RESET OUTPUT
        var removestuff = GameObject.FindGameObjectsWithTag("Output");

        for (var i = 0; i < removestuff.Length; i++)
        {
            Destroy(removestuff[i]);
        }
        outputui.GetComponent<Canvas>().enabled = false;
        
        
        
    }

    void Download()
    {
        WebClient docdown = new WebClient();


        var DLobjects = GameObject.FindGameObjectsWithTag("Output");
        for (var i = 0; i < DLobjects.Length; i++)
        {

            if ( DLobjects[i].GetComponentInChildren<Toggle>().isOn == true)
            {


                string seclink = DLobjects[i].transform.FindChild("OutputtextText").GetComponent<Text>().text;
                Debug.Log(seclink);

                //GET actual document link
                string qkget = docdown.DownloadString(seclink);//docdown.DownloadString(DLobjects[i].transform.FindChild("OutputtextText").GetComponent<Text>().text);

                Regex findqk = new Regex(@"<td scope=""row"">1</td>\s{13}<td scope=""row"">.*</td>\s{13}<td scope=""row""><a href=""/Archives/edgar/data/[0-9]*/[0-9]*/[a-zA-Z0-9_\-]*.\w{3}", RegexOptions.IgnoreCase);

                Match qkmatch = findqk.Match(qkget);

                string documenturl =Regex.Replace(qkmatch.ToString(), @"<td scope=""row"">1</td>\s{13}<td scope=""row"">.*</td>\s{13}<td scope=""row""><a href=""", "");
               



                if (documenturl.Length == 0)
                {
                    Debug.Log("Reverting to text crap");
                    documenturl = "http://www.sec.gov/" + new Regex(@"Archives/edgar/data/[0-9\-\/]*.txt").Match(qkget).ToString();
                    string document = docdown.DownloadString(documenturl);
                    string savename = tickersymbol + "-" + DLobjects[i].transform.FindChild("YearText").GetComponent<Text>().text + "-" + DLobjects[i].transform.FindChild("FilingTypeText").GetComponent<Text>().text;
                    
                    // Checks if document is Amendment, If it is the save name will contain a / and that messes stuff up. So remove it
                    if (savename.Trim().EndsWith("A"))
                    {
                        savename = savename.Substring(0, savename.Length - 2) + "A";
                    }

                    System.IO.StreamWriter file = new System.IO.StreamWriter(@"E:\Haitham\Documents\New folder\" + savename + ".txt");


                    file.WriteLine(document);
                    file.Close();
                    file.Dispose();
                }
                else
                {

                    string document = docdown.DownloadString("http://www.sec.gov" + documenturl);
                    string savename = tickersymbol + "-" + DLobjects[i].transform.FindChild("YearText").GetComponent<Text>().text + "-" + DLobjects[i].transform.FindChild("FilingTypeText").GetComponent<Text>().text;

                    //Checks if document is Amendment, If it is the save name will contain a / and that messes stuff up. So remove it
                    if (savename.Trim().EndsWith("A"))
                        {
                        savename = savename.Substring(0, savename.Length - 2) + "A";
                        }

                    System.IO.StreamWriter file = new System.IO.StreamWriter(@"E:\Haitham\Documents\New folder\" + savename + ".htm");


                    file.WriteLine(document);
                    file.Close();
                    file.Dispose();

                }

                docdown.Dispose();
            }
        }
    }

    void Update()
    {
       
    }
    
}
