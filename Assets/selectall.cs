﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class selectall : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        

	}

    void Toggle()
    {
        if (gameObject.GetComponent<Toggle>().isOn == true)
        {
            var othertoggles = GameObject.FindGameObjectsWithTag("Toggle");
            for (var i=0;i<othertoggles.Length; i++)
            {
                othertoggles[i].GetComponentInChildren<Toggle>().isOn = true;
            }
        }
        else
        {
            var othertoggles = GameObject.FindGameObjectsWithTag("Toggle");
            for (var i = 0; i < othertoggles.Length; i++)
            {
                othertoggles[i].GetComponentInChildren<Toggle>().isOn = false;
            }
        }
    }
}
