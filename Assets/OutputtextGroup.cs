﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class OutputtextGroup : MonoBehaviour {

    string rawtext;
    string docid;
    public static string year;
    public static string filingtype;
    public Transform outputTextTextBox;
    public Transform DocIDBox;
    public Transform YearBox;
    public Transform typebox;
    

	// Use this for initialization
	void Start () {

        //outputTextTextBox = transform.FindChild("OutputtextText");                  //Get DocID Object
        rawtext = outputTextTextBox.GetComponentInChildren<Text>().text;            //Get URL from Docid


        // SET DOC ID STUFF
        docid = new Regex(@"[0-9]*-[0-9]{2}-[0-9]*").Match(rawtext).ToString();     //Strip SEC ID from URl
        DocIDBox.GetComponent<Text>().text = docid;                                 //Set Doc ID

        //Set YEAR STUFf
        YearBox.GetComponent<Text>().text = year;

        //Set filing Type stuff
        typebox.GetComponent<Text>().text = filingtype;

        

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
